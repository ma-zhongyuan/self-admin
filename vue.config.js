//path引入 直接引用 node中自带的模块
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  lintOnSave: false, //在保存时不启用eslint检查代码
  publicPath: "/",
  // outputDir: process.env.outputDir,
  outputDir: "dist",
  devServer: {
    open: true, //编译完成是否自动打开网页 自动启动
    host: "0.0.0.0", // 指定使用地址，默认localhost,0.0.0.0代表可以被外界访问
    https: false, //编译失败时刷新页面
    hot: true, // 开启热加载
    port: 8080, //访问端口
    proxy: {
      //配置代理
      [process.env.VUE_APP_BASE_API]: {
        target: process.env.VUE_APP_PROXY_TARGET,
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          ["^" + process.env.VUE_APP_BASE_API]: "",
        },
      },
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        // path.resolve:方法会把一个路径或路径片段的序列解析为一个绝对路径。
        //console.log(path.resolve("/目录1", "/目录2", "目录3"));
        // /目录2/目录3
        // "@": path.resolve(__dirname, "src"),
        // "~": path.resolve(__dirname, "src"),
        "@": resolve("src"),
        "~": resolve("src"),
        _c: resolve("src/components"),
        _a: resolve("src/api"),
        _u: resolve("src/utils"),
      },
    },
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require("postcss-pxtorem")({
            rootValue: 16, // 换算的基数
            selectorBlackList: [], // 忽略转换正则匹配项
            propList: ["*"],
          }),
        ],
      },
    },
  },
  chainWebpack: (config) => {
    // 找到svg-loader
    const svgRule = config.module.rule("svg");
    // 清除已有的loader, 如果不这样做会添加在此loader之后
    svgRule.uses.clear();
    // 正则匹配排除node_modules目录
    svgRule.exclude.add(/node_modules/);
    svgRule.include.add(resolve("src/assets/icons"));
    // 添加svg新的loader处理
    svgRule
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .tap((options) => {
        options = {
          symbolId: "icon-[name]",
        };
        return options;
      });

    // 其他svg图片，我们还是想当作图片使用

    // 修改images loader 添加svg处理
    const imagesRule = config.module.rule("images");
    imagesRule.exclude.add(resolve("src/assets/icons"));
    config.module.rule("images").test(/\.(png|jpe?g|gif|svg)(\?.*)?$/);
  },
};
