module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  //按需加载 element 组件 新增代码如下
  plugins: [
    [
      "component",
      {
        libraryName: "element-ui",
        styleLibraryName: "theme-chalk",
      },
    ],
  ],
};
