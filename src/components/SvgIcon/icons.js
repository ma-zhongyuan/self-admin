import Vue from "vue";
import SvgIcon from "./index"; // svg组件

// register globally
Vue.component("SvgIcon", SvgIcon);

const requireAll = (requireContext) =>
  requireContext.keys().map(requireContext);
const req = require.context("@/assets/icons/svg", false, /\.svg$/); //动态引入所有文件
requireAll(req);

// require.context函数接受三个参数:
//
// directory {String} -读取文件的路径
// useSubdirectories {Boolean} -是否遍历文件的子目录
// regExp {RegExp} -匹配文件的正则
