// 导入自己需要的组件
import {
  // Select,
  // Option,
  // OptionGroup,
  // Input,
  // Tree,
  // Dialog,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  RadioGroup,
  RadioButton,
  Row,
  Col,
  Button,
  Breadcrumb,
  BreadcrumbItem,
  Table,
  TableColumn,
  Form,
  FormItem,
  Tooltip,
  Input,
  Tabs,
  TabPane,
} from "element-ui";
// import {ElTabs} from "element-ui/types/tabs";
// import {ElTabPane} from "element-ui/types/tab-pane";

const coms = [
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  RadioGroup,
  RadioButton,
  Row,
  Col,
  Button,
  Breadcrumb,
  BreadcrumbItem,
  Table,
  TableColumn,
  Form,
  FormItem,
  Tooltip,
  Input,
  Tabs,
  TabPane,
];
//
// export default {
//   install(Vue, options) {
//     coms.map((c) => {
//       Vue.component(c.name, c);
//     });
//   },
// };

const element = {
  //这里的 install 方法表示在 main.js 中，如果使用 Vue.use() 方法的话，则该方法默认会调用 install 方法
  install: function (Vue) {
    // Vue.use(Select); //这里要使用 Select 组件，必须同时使用 Option 和 OptionGroup
    // Vue.use(Option);
    coms.map((c) => {
      Vue.component(c.name, c);
    });
  },
};
export default element;
