1、在标签内作为正文内容
 <div>{{$t("admin")}}</div>

2、作为标签属性使用
<input :placeholder="$t('test')">

3、作为js中文字使用
console.log(this.$t("language.zh"))


改变语言
this.$i18n.locale = "zh";
