import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/layout/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/daping",
  },
  // {
  //   path: "/",
  //   // component: () =>
  //   //   import(/* webpackChunkName: "about" */ "@/views/dashboard/index.vue"),
  //   component: Layout,
  //   redirect: "/dashboard",
  //   children: [
  //     {
  //       path: "dashboard",
  //       component: () => import("@/views/dashboard/index"),
  //       name: "Dashboard",
  //       meta: { title: "首页", icon: "dashboard", affix: true },
  //     },
  //   ],
  // },
  {
    path: "/login",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/login/index.vue"),
  },
  {
    path: "/certificateToPdf",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/certificateToPdf.vue"),
  },
  {
    path: "/daping",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/daping/index.vue"),
  },
  {
    path: "/datav",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/datav/index.vue"),
  },
  {
    path: "/tableSelect",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/table/tableSelect.vue"),
  },
  {
    path: "/testTableSelect",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/table/testTableSelect.vue"),
  },
  {
    path: "/pressToImg",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/pressToImg.vue"),
  },
  {
    path: "/testSequence",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/sequenceDiagramTest/index.vue"),
  },
  {
    path: "/userCenterChart",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/userCenterChart/index.vue"),
  },
  {
    path: "/gaode",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/maps/gaodeMap.vue"),
  },
  {
    path: "/editor",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/ueditor.vue"),
  },
  {
    path: "/chartTest",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/chartTest.vue"),
  },
  {
    path: "/xinghaoTest",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/xinghaoTest.vue"),
  },
  {
    path: "/testChart",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/testChart.vue"),
  },
  {
    path: "/tencenMapTest",
    component: () =>
        import(/* webpackChunkName: "about" */ "@/views/tencentMapTest.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
