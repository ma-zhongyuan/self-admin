
export const lineChat = function (
    data = {
        xdata: ['1月','2月','3月','4月','5月','6月','7月'],
        ydata: [6200,9122,7100,6000,4720,3823,5500],
    },color={
        offset0Color:'rgba(56, 96, 245, 0.25)',
        offset1Color:'rgba(56, 96, 245, 0)',
    }
) {
    return {
        tooltip: {
            trigger: "axis",
            formatter: function (params) {
                return (
                    '<div style="color: #000000;">'+params[0].marker+params[0].name +' '+params[0].data+'万立方米'+'</div>'
                );
            },
            axisPointer: {
                animation: false,
                lineStyle:{
                    width:1,
                    type:'solid',
                    color:'#6893ED'
                }
            },
        },
        grid: {
            left: 60,
            right: 10,
            top:30,
            bottom: 150,
        },
        xAxis: {
            type: "category",
            data: data.xdata,
            axisLine: {
                // x轴坐标轴线
                lineStyle: {
                    show: true,
                    color: "rgba(255,255,255,0.1)",
                    width: 1,
                },
            },
            axisTick: {
                show: true,//隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 16,
                textStyle: {
                    color: "#A5A8B8" //X轴文字颜色
                }
            },
        },
        yAxis: {
            name:'单位：分',
            nameTextStyle: {
                color: "#A5A8B8",
                fontSize: 12,
                verticalAlign:'bottom',
                padding:[0,60,0,0],
            },
            nameLocation:'end',
            type: "value",
            splitLine: {
                lineStyle: {
                    show: true,
                    color: "rgba(255,255,255,0.1)",
                    width: 1,
                }
            },
            axisTick: {
                show: false,
            },
            axisLine: {
                show: false,
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 16,
                textStyle: {
                    color: "#A5A8B8" //X轴文字颜色
                }
            },
        },
        series: [
            {
                name: "",
                type: "line",
                data: data.ydata,
                smooth: true,//是否平滑曲线显示
                symbol: "circle",
                symbolSize: 5,
                showSymbol: false,//是否显示圆圈
                lineStyle: {
                    color: "#3860F5",
                },
                areaStyle: {
                    color: {
                        type: "linear",
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [
                            {
                                offset: 0,
                                // color: "rgba(96,147,249,0.39)",
                                color: color.offset0Color
                            },
                            // {
                            //     offset: 0.17,
                            //     color: "rgba(96,147,249,0.22)",
                            // },
                            // {
                            //     offset: 0.53,
                            //     color: "rgba(96,147,249,0.08)",
                            // },
                            {
                                offset: 1,
                                // color: "rgba(96,147,249,0.00)",
                                color: color.offset1Color
                            },
                        ],
                    },
                },
            },
        ],
    }

}
