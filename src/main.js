import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import dataV from "@jiaminghi/data-view"

//引入按需引入的element文件
import element from "./element";
//全局引入element-ui样式
import "element-ui/lib/theme-chalk/index.css";
import VXETable from "vxe-table";
import 'vxe-table/lib/style.css'
//配置 全局初始化样式
import "@/assets/css/reset.scss";
//导出成pdf
import htmlToPdf from "_u/htmlToPdf";
//引入rem适配
import "_u/rem/page-scale.js";
import "@/utils/validate.form";

//注册
Vue.use(element);
Vue.use(dataV);
Vue.use(VXETable);
Vue.use(htmlToPdf);

//引入i18n国际化

import i18n from "./i18n";

import "_c/SvgIcon/icons";

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    i18n, //将i18n挂载至vue实例
    render: (h) => h(App),
}).$mount("#app");
