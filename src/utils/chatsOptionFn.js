import * as echarts from 'echarts'

// 火电装机容量 柱状图配置项
export const barChat = function (
    data = {
        xdata: ['外二', '外三', '吴二', '临港', '奉贤', '吴忠', '崇明', '平二', '品一'],
        ydata: [40, 59, 32, 85, 65, 33, 54, 58, 20, 80]
    }
) {
    return {
        grid: {
            left: 50,
            right: 40,
            top: 30,
            bottom: 30,
        },
        xAxis: {
            type: "category",
            data: data.xdata,
            axisLine: {
                show:true,
                lineStyle: {
                    color: "#cccccc",
                    width: 2,
                },
            },
            axisTick: {
                show: false,//隐藏X轴刻度
            },
            axisLabel: {
                show:true,
                color: "#fff",
                fontSize: 11,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                }
                // * 以下用来设置 x轴 不自适应隐藏
                // interval: 0,
            },
        },
        yAxis: {
            name:'万立方米',
            nameTextStyle: {
                color: "#4D4D4D",
                fontSize: 12,
                verticalAlign:'bottom',
                padding:[0,20,0,0],
            },
            nameLocation:'end',
            type: "value",
            interval: 25,
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#cccccc',
                    width: 0.5
                },
            },
            axisLabel: {
                show: true,
                margin: 14,
                fontSize: 11,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                }
            },
            axisTick: {
                show: false,
            },
            axisLine: {
                show: false,
            },
        },
        series: {
            type: "bar",
            data: data.ydata,
            barWidth: "35%",
            label: {
                show: false,
            },
            itemStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(
                        0,
                        1,
                        0,
                        0,
                        [
                            {
                                offset: 0.98,
                                color: 'rgba(165,193,250,0.71)',
                            },
                            {
                                offset: 0.08,
                                color: "#3375EB",
                            },
                        ],
                        false
                    ),
                },
            },
        },
    }

}

// 天然气入网供应量 块柱状图配置项
export const zkBarChat = function (
    data = {
        xdata: [
            "上海RNG",
            "西气+",
            "东气",
            "川气",
            "五号沟RNG气量化",
            "其他",
            "五号沟月末库存量",
        ],
        ydata: [60, 70, 89,100, 41, 58, 72]
    }
) {
    return {
        tooltip:{},
        animation: false,
        grid: {
            top: "15%",
            bottom: "16%"//也可设置left和right设置距离来控制图表的大小
        },
        xAxis: {
            data: data.xdata,
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#cccccc',
                    width:2
                }
            },
            axisTick: {
                show: false //隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D", //X轴文字颜色
                },
                interval: 0,//使x轴文字显示全
                formatter: function(params) {
                    var newParamsName = "";
                    var paramsNameNumber = params.length;
                    var provideNumber = 5; //一行显示几个字
                    var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                    if (paramsNameNumber > provideNumber) {
                        for (var p = 0; p < rowNumber; p++) {
                            var tempStr = "";
                            var start = p * provideNumber;
                            var end = start + provideNumber;
                            if (p == rowNumber - 1) {
                                tempStr = params.substring(start, paramsNameNumber);
                            } else {
                                tempStr = params.substring(start, end) + "\n";
                            }
                            newParamsName += tempStr;
                        }
                    } else {
                        newParamsName = params;
                    }
                    return newParamsName;
                }
            },
        },
        yAxis: [
            {
                name:'TWh',
                nameTextStyle: {
                    color: "#4D4D4D",
                    fontSize: 12,
                    verticalAlign:'bottom',
                    padding:[0,45,0,0],
                },
                type: "value",
                gridIndex: 0,
                // min: 0,
                // max: 100,
                interval: 25,
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#cccccc',
                        width: 0.5
                    },
                },
                axisTick: {
                    show: false
                },
                axisLine: {// 是否显示y轴
                    show: false,
                },
                axisLabel: {
                    show: true,
                    margin: 10,
                    fontSize: 13,
                    textStyle: {
                        color: "#4D4D4D" //X轴文字颜色
                    },
                },
            },
        ],
        series: [
            {
                name: "",
                type: "bar",
                barWidth: 20,
                itemStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: "#A5C1FA"
                            },
                            {
                                offset: 1,
                                color: "#3375EB"
                            },
                        ])
                    }
                },
                data: data.ydata,
                z: 10,
                zlevel: 0,
                label: {
                    show: false,
                }
            },
            {
                // 分隔
                type: "pictorialBar",
                itemStyle: {
                    normal:{
                        color:"#ffffff"
                    }
                },
                symbolRepeat: "fixed",
                symbolMargin: 6,
                symbol: "rect",
                symbolClip: true,
                symbolSize: [20, 2],
                symbolPosition: "start",
                symbolOffset: [0, -1],
                // symbolBoundingData: this.total,
                data: data.ydata,
                width: 25,
                z: 0,
                zlevel: 1,
            },
        ]
    }

}

// 综合统计 渐变折线图
export const jbLineChat = function (
    data = {
        xdata: ['1月','2月','3月','4月','5月','6月','7月'],
        ydata: [6200,9122,7100,6000,4720,3823,5500],
    }
) {
    return {
        tooltip: {
            trigger: "axis",
            formatter: function (params) {
                return (
                    '<div style="color: #000000;">'+params[0].marker+params[0].name +' '+params[0].data+'万立方米'+'</div>'
                );
            },
            axisPointer: {
                animation: false,
                lineStyle:{
                    width:1,
                    type:'solid',
                    color:'#6893ED'
                }
            },
        },
        grid: {
            left: 55,
            right: 10,
            top:30,
            bottom: 40,
        },
        xAxis: {
            type: "category",
            data: data.xdata,
            axisLine: {
                // x轴坐标轴线
                lineStyle: {
                    show: true,
                    color: "#DEE2E7",
                    width: 1,
                },
            },
            axisTick: {
                show: false,//隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                }
            },
        },
        yAxis: {
            name:'万立方米',
            nameTextStyle: {
                color: "#4D4D4D",
                fontSize: 12,
                verticalAlign:'bottom',
                padding:[0,60,0,0],
            },
            nameLocation:'end',
            type: "value",
            splitLine: {
                show: false,
            },
            axisTick: {
                show: false,
            },
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#DEE2E7',
                    width:1
                }
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                }
            },
        },
        series: [
            {
                name: "",
                type: "line",
                data: data.ydata,
                smooth: true,//是否平滑曲线显示
                symbol: "circle",
                symbolSize: 5,
                showSymbol: false,//是否显示圆圈

                // symbol: "circle",
                // symbolSize: 6,
                // itemStyle: {
                //     normal: {
                //         color: "#fff",
                //         borderColor: "#6893ED",
                //         borderWidth: 6,
                //     },
                //     emphasis: {
                //         color: "#6893ED",
                //     },
                // },
                lineStyle: {
                    color: "#6093F9",
                },
                areaStyle: {
                    color: {
                        type: "linear",
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [
                            {
                                offset: 0,
                                color: "rgba(96,147,249,0.39)",
                            },
                            // {
                            //     offset: 0.17,
                            //     color: "rgba(96,147,249,0.22)",
                            // },
                            // {
                            //     offset: 0.53,
                            //     color: "rgba(96,147,249,0.08)",
                            // },
                            {
                                offset: 1,
                                color: "rgba(96,147,249,0.00)",
                            },
                        ],
                    },
                },
            },
        ],
    }

}

// 集团发电详情 三角柱
export const sjzBarOption = function (
    data = {
        xdata: ['销售公司+','郊区','宝钢','化工区 （不含漕泾）','石化+','燃机电厂+','气态反输'],
        ydata: [60,80,68,40,68,97,62],
    }
) {
    return {
        tooltip: {
            trigger: "axis",
            axisPointer: {
                type: "none",
            },
            formatter: function (params) {
                return params[0].name + ": " + params[0].value;
            },
        },
        grid: {
            top: "15%",
            bottom: "15%"//也可设置left和right设置距离来控制图表的大小
        },
        xAxis: {
            data: data.xdata,
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#1A5C6F',
                    width:2
                }
            },
            axisTick: {
                show: false //隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                },
                interval: 0,//使x轴文字显示全
                formatter: function(params) {
                    var newParamsName = "";
                    var paramsNameNumber = params.length;
                    var provideNumber = 5; //一行显示几个字
                    var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                    if (paramsNameNumber > provideNumber) {
                        for (var p = 0; p < rowNumber; p++) {
                            var tempStr = "";
                            var start = p * provideNumber;
                            var end = start + provideNumber;
                            if (p == rowNumber - 1) {
                                tempStr = params.substring(start, paramsNameNumber);
                            } else {
                                tempStr = params.substring(start, end) + "\n";
                            }
                            newParamsName += tempStr;
                        }
                    } else {
                        newParamsName = params;
                    }
                    return newParamsName;
                }
            },
        },
        yAxis: {
            name:'TWh',
            nameTextStyle: {
                color: "#4D4D4D",
                fontSize: 12,
                verticalAlign:'bottom',
                padding:[0,45,0,0],
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#cccccc',
                    width: 0.5
                },
            },
            axisTick: {
                show: false,
            },
            axisLine: {
                show: false,
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D" //X轴文字颜色
                }
            },
        },

        series: [
            {
                name: "hill",
                type: "pictorialBar",
                barCategoryGap: "50%",
                symbol: "triangle",
                color: {
                    type: "linear",
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [
                        {
                            offset: 0,
                            color: "#578FF4", // 0% 处的颜色
                        },
                        {
                            offset: 1,
                            color: "#C6D8FE", // 100% 处的颜色
                        },
                    ],
                    globalCoord: false, // 缺省为 false
                },
                data: data.ydata,
            },
        ],
    };

}

// 圆环
export const ringOption = function (
    data = {
        color:['#7AB4CE','#6093F9'],
        dataArr:[{name:'受伤',value:1},{name:'死亡',value:1}],
        position:["43%", "50%"],
        radius1:["50%", "70%"],
        radius2:["80%"],
        radius3:["40%"],
        show:false
    }
) {
    return {
        color:data.color,
        polar: {
            radius: ["42%", "52%"],
            center: data.position
        },
        legend: {
            // width: '100%',
            show:data.show,
            // // orient: 'vertical',  // 图例列表的布局朝向
            // bottom: 20,//调整图例位置
            itemHeight: 9,
            itemWidth: 9,//修改icon图形大小
            icon: 'circle',//图例前面的图标形状
            // // width:10,
            //
            // itemGap: 20, // 图例之间的间隔，横向布局时为水平间隔，纵向布局时为纵向间隔
            // orient: 'vertical',
            // right: '0',
            // top: '10',

            bottom: '0%',
            left: '15%',
            width: 440,
            itemGap: 10,
            textStyle: {
                width: 80,
                backgroundColor: 'transparent',
                padding: [14, 10, 10, 10],
                color:'#333333'
            }
        },
        angleAxis: {
            max: 100,
            show: false,
        },
        radiusAxis: {
            type: "category",
            show: true,
            axisLabel: {
                show: false,
            },
            axisLine: {
                show: false,
            },
            axisTick: {
                show: false,
            },
        },
        series: [
            {
                type: "pie",
                radius: data.radius1,
                center: data.position,
                hoverAnimation: false,
                // avoidLabelOverlap: false,
                z: 10,
                itemStyle: {
                    normal: {
                        borderWidth: 5,
                        // borderRadius: 60, // 设置每一段子项目的圆角
                    },
                },
                label: {
                    show: false,
                },
                data: data.dataArr,
                labelLine: {
                    show: false,
                },
            },

            {
                type: "pie",
                radius: data.radius2,
                hoverAnimation: false,
                center: data.position,
                itemStyle: {
                    color: "#ffffff",
                    borderWidth: 1,
                    borderColor: "#6093F9",
                },
                data: [100],
            },
            {
                type: "pie",
                radius: data.radius3,
                hoverAnimation: false,
                center: data.position,
                itemStyle: {
                    color: "#ffffff",
                    borderWidth: 1,
                    borderColor: "#6093F9",
                },
                data: [100],
            },
        ],
    };
}

// 双折线
export const twoLineOption = function (
    data = {
        xdata: ['1月','2月','3月','4月','5月','6月','7月'],
        ydata1: [160,290,210,220,140,155,200],
        ydata2: [300,360,300,280,260,250,270],
    }
) {
    return {
        title: {
            text: "社会用气事故",
            x: "center",
            y: "-5",
            textStyle: {
                fontSize: 16,
                color: "#fff",
            },
        },
        legend: {
            data: ["受伤", "死亡"],
            bottom: 0,//调整图例位置
            itemHeight: 6,
            itemWidth: 9,//修改icon图形大小
            icon: 'roundRect',//图例前面的图标形状
            textStyle: {//图例文字的样式
                color: '#4D4D4D',
                fontSize: 13,
                padding:[10,10,10,10]
            },
            itemGap:90,//图例每项之间的间隔
        },
        tooltip: {
            trigger: "axis",
        },
        grid: {
            top: "20%",
            bottom: "25%"//也可设置left和right设置距离来控制图表的大小
        },
        xAxis: {
            data: data.xdata,
            axisLine: {
                show: true,
                lineStyle: {
                    color: '#DEE2E7',
                    width:1
                }
            },
            axisTick: {
                show: false //隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                margin: 10,
                fontSize: 13,
                textStyle: {
                    color: "#4D4D4D", //X轴文字颜色
                }
            },

        },
        yAxis: [
            {
                name:'TWh',
                nameTextStyle: {
                    color: "#4D4D4D",
                    fontSize: 12,
                    verticalAlign:'bottom',
                    padding:[0,45,0,0],
                },
                nameLocation:'end',
                type: "value",
                gridIndex: 0,
                // min: 0,
                // max: 100,
                // interval: 50,
                splitLine: {
                    show: false,
                },
                axisTick: {
                    show: false
                },
                axisLine: {// 是否显示y轴
                    show: true,
                    lineStyle: {
                        color: '#DEE2E7',
                        width:1
                    }
                },
                axisLabel: {
                    show: true,
                    margin: 10,
                    fontSize: 13,
                    textStyle: {
                        color: "#4D4D4D" //X轴文字颜色
                    }
                },
            },
        ],
        series: [
            {
                name: "受伤",
                data: data.ydata2,
                type: "line",
                smooth: true, //折线是否平滑
                areaStyle: {
                    opacity: 0,
                },
                itemStyle: {
                    normal: {
                        color: "#6093F9", //小圆点的颜色
                        lineStyle: {
                            color: "#6D9CFD", //折线的颜色
                        },
                    },
                },
            },
            {
                name: "死亡",
                data: data.ydata1,
                type: "line",
                smooth: true, //是否平滑
                areaStyle: {
                    opacity: 0,
                },
                itemStyle: {
                    normal: {
                        color: "#86BCD5", //小圆点的颜色
                        lineStyle: {
                            color: "#86BCD5", //折线的颜色
                        },
                    },
                },
            },
        ],
    };
    ;

}

// 圆环2
export const ringOptionNew = function (
    data = {
        color:['#7AB4CE','#6093F9'],
        dataArr:[{name:'受伤',value:1},{name:'死亡',value:1}],
        position:["43%", "50%"],
        radius1:["43", "57"],
        radius2:["63", "64"],
        radius3:["35", "36"],
        show:false
    }
) {
    return {
        title:{
            show:data.show,
            text:'各单位天然气购销差占比',
            x:'center',
            y:'53%',
            textStyle:{
                fontSize:16,
                color:'#000000',
                fontWeight:500
            }
        },
        legend: {
            show:data.show,
            // // orient: 'vertical',  // 图例列表的布局朝向
            // bottom: 20,//调整图例位置
            itemHeight: 9,
            itemWidth: 9,//修改icon图形大小
            icon: 'circle',//图例前面的图标形状
            // // width:10,
            //
            // itemGap: 20, // 图例之间的间隔，横向布局时为水平间隔，纵向布局时为纵向间隔
            // orient: 'vertical',
            // right: '0',
            // top: '10',

            bottom: '0%',
            left: '15%',
            width: 440,
            itemGap: 10,
            textStyle: {
                width: 80,
                backgroundColor: 'transparent',
                padding: [14, 10, 10, 10],
                color:'#333333'
            }
        },
        color: data.color,
        tooltip: {
            trigger: "item",
            show: true,
        },
        series: [
            {
                // color: data.color,
                type: "pie",
                radius: data.radius1,
                center: data.position,

                data: data.dataArr,
                label: {
                    show: false,
                },
            },
            {
                type: "pie",
                radius: data.radius2,
                data: [100],
                center: data.position,

                label: {
                    show: false,
                },
            },
            {
                type: "pie",
                radius: data.radius3,
                data: [100],
                center: data.position,
                label: {
                    show: false,
                },
            },
        ],
    };
    ;
}


export const zoomLineChartOption = function (){
    let base = +new Date(1988, 9, 3);
    let oneDay = 24 * 3600 * 1000;
    let data = [[base, Math.random() * 300]];
    for (let i = 1; i < 20000; i++) {
        let now = new Date((base += oneDay));
        data.push([+now, Math.round((Math.random() - 0.5) * 20 + data[i - 1][1])]);
    }

    let option = {
        tooltip: {
            trigger: 'axis',
            position: function (pt) {
                return [pt[0], '10%'];
            }
        },
        title: {
            left: 'center',
            text: 'Large Ara Chart'
        },
        toolbox: {
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                restore: {},
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'time',
            boundaryGap: false
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%']
        },
        dataZoom: [
            {
                type: 'inside',
                start: 0,
                end: 20
            },
            {
                start: 0,
                end: 20
            }
        ],
        series: [
            {
                name: 'Fake Data',
                type: 'line',
                smooth: true,
                symbol: 'none',
                areaStyle: {},
                data: data
            }
        ]
    };

    return option
}